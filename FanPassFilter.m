img = imread('download.jpg');
imgcopy = img(:,:,1);

figure, imagesc(img);
axis square;
colormap gray;
title('Original Image');
set(gca, 'XTick', [], 'YTick', []);

padded = zeros(168*3, 300*3);
k = 1;
l = 1;
for i = 169:168*2
    for j = 301:600
        padded(i, j) = imgcopy(k, l);
        l = l+1;
    end
    k = k+1;
    l = 1;
end


fftImage = fft2(double(padded));
fftShifted = fftshift(fftImage);

padded = zeros(168*3, 300*3);

mag = abs(fftShifted);
phase = angle(fftShifted);

centre = [(168*3)/2 (300*3)/2]';
radius = 250;

angle1 = 0;
angle2 = 40;

x1 = ((168*3)/2) + radius * cosd(angle1);
y1 = ((300*3)/2) + radius * sind(angle1);

x2 = ((168*3)/2) + radius * cosd(angle2);
y2 = ((300*3)/2) + radius * sind(angle2);

x3 = ((168*3)/2) + radius * cosd(angle1 + 180);
y3 = ((300*3)/2) + radius * sind(angle1 + 180);

x4 = ((168*3)/2) + radius * cosd(angle2 + 180);
y4 = ((300*3)/2) + radius * sind(angle2 + 180);

vec1 = [x1 y1]' - centre;
vec2 = [x2 y2]' - centre;
vec3 = [x3 y3]' - centre;
vec4 = [x4 y4]' - centre;

angleBetweenFan = acosd(dot(vec3, vec4) / (norm(vec3) * norm(vec4)));

for i=1:168*3
    for j=1:300*3
        pointVec = [i, j]';
        pointsDistance = sqrt((pointVec(1, 1) - centre(1, 1))^2 + (pointVec(2, 1) - centre(2, 1))^2);
        pointV1Angle = acosd(dot(vec1, pointVec - centre) / (norm(vec1) * norm(pointVec - centre)));
        pointV2Angle = acosd(dot(vec2, pointVec - centre) / (norm(vec2) * norm(pointVec - centre)));
        pointV3Angle = acosd(dot(vec3, pointVec - centre) / (norm(vec3) * norm(pointVec - centre)));
        pointV4Angle = acosd(dot(vec4, pointVec - centre) / (norm(vec4) * norm(pointVec - centre)));
        if(pointV1Angle > angleBetweenFan || pointV2Angle > angleBetweenFan)
            if(pointV3Angle > angleBetweenFan || pointV4Angle > angleBetweenFan)
                mag(i, j) = 0;
            elseif(pointsDistance > radius)
                mag(i, j) = 0;
            end
        elseif(pointsDistance > radius)
            mag(i, j) = 0;
        end
    end 
end

rebuiltFFT = mag.*exp(1i*phase);

unshiftedFFT = ifftshift(rebuiltFFT);

invFFTImage = ifft2(unshiftedFFT);

origImage = abs(invFFTImage);

finalImage = zeros(168, 300);

for i = 1:168
    for j=1:300
        finalImage(i, j) = origImage(i+168, j+300);
    end
end

figure, imagesc(finalImage);
axis square;
colormap gray;
title('Filtered Image');
set(gca, 'XTick', [], 'YTick', []);

