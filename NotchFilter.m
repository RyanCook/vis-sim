img = imread('download.jpg');
imgcopy = img(:,:,1);

figure, imagesc(img);
axis square;
colormap gray;
title('Original Image');
set(gca, 'XTick', [], 'YTick', []);

padded = zeros(168*3, 300*3);
k = 1;
l = 1;
for i = 169:168*2
    for j = 301:600
        padded(i, j) = imgcopy(k, l);
        l = l+1;
    end
    k = k+1;
    l = 1;
end


fftImage = fft2(double(padded));
fftShifted = fftshift(fftImage);

padded = zeros(168*3, 300*3);

mag = abs(fftShifted);
phase = angle(fftShifted);

centre = [(168*3)/2 (300*3)/2]';
radius = 180;
innerRadius = 100;


for i=1:168*3
    for j=1:300*3
        pointVec = [i, j]';
        pointsDistance = sqrt((pointVec(1, 1) - centre(1, 1))^2 + (pointVec(2, 1) - centre(2, 1))^2);
        if(pointsDistance < radius && pointsDistance > innerRadius)
           mag(i, j) = 0;
        end
    end 
end

rebuiltFFT = mag.*exp(1i*phase);

unshiftedFFT = ifftshift(rebuiltFFT);

invFFTImage = ifft2(unshiftedFFT);

origImage = abs(invFFTImage);

finalImage = zeros(168, 300);

for i = 1:168
    for j=1:300
        finalImage(i, j) = origImage(i+168, j+300);
    end
end

figure, imagesc(finalImage);
axis square;
colormap gray;
title('Filtered Image');
set(gca, 'XTick', [], 'YTick', []);

